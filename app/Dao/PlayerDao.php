<?php
/**
 * Created by PhpStorm.
 * User: pablo.juarez
 * Date: 10/12/2018
 * Time: 3:44 PM
 */

namespace App\Dao;


use App\Models\Player;

class PlayerDao
{
    /**
     * Store a new player entity
     *
     * @param $data
     * @return Player
     */
    public static function store($data)
    {
        $player = new Player;
        $player->fill($data);
        $player->save();
        return $player;
    }

    /**
     * @param $playerId
     * @param $data
     * @return mixed
     */
    public static function update($playerId, $data)
    {
        $player = Player::find($playerId);
        $player->fill($data);
        $player->save();
        return $player;
    }

    /**
     * Retrieve a player model by an id given
     *
     * @param $playerId
     * @return mixed
     */
    public static function getById($playerId)
    {
        return Player::where('id', $playerId)->with('teamInfo')->first();
    }

    /**
     * Retrieve a players collection by a team id given
     * @param $teamId
     * @return mixed
     */
    public static function getByTeam($teamId)
    {
        return Player::select('id', 'first_name', 'last_name', 'alias')->where('team', $teamId)->get();
    }

}