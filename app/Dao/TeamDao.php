<?php
/**
 * Created by PhpStorm.
 * User: pablo.juarez
 * Date: 10/12/2018
 * Time: 2:24 PM
 */

namespace App\Dao;


use App\Models\Team;

class TeamDao
{
    /**
     * save a new team entity
     *
     * @param array $data
     * @return Team
     */
    public static function store($data)
    {
        $team = new Team;
        $team->fill($data);
        $team->save();
        return $team;
    }

    /**
     * Retrieve team model by id
     *
     * @param int $id
     * @return mixed
     */
    public static function getById($id)
    {
        return Team::find($id);
    }

}