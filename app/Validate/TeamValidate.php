<?php
/**
 * Created by PhpStorm.
 * User: pablo.juarez
 * Date: 10/12/2018
 * Time: 12:28 PM
 */

namespace App\Validate;


class TeamValidate
{
    use Validate;

    /**
     * Validate entry to add a new team
     *
     * @param $data
     * @return bool
     * @throws \App\Exceptions\ApiException
     */
    public static function newTeam($data)
    {
        $rules = [
           'name' => 'required|unique:teams'
        ];
        Validate::validate($data, $rules);
        return true;
    }

    /**
     * Validate if team exists by an id given
     *
     * @param $id
     * @throws \App\Exceptions\ApiException
     */
    public static function exists($id)
    {
        $data = [ 'id' => $id ];
        $rules = [
            'id' => 'required|exists:teams'
        ];
        Validate::validate($data, $rules);
    }

}