<?php
/**
 * Created by PhpStorm.
 * User: pablo.juarez
 * Date: 10/12/2018
 * Time: 2:19 PM
 */

namespace App\Validate;

use Illuminate\Support\Facades\Validator;
use App\Exceptions\ApiException;

trait Validate
{
    /**
     * Validate a data set with a collection of rules
     *
     * @param array $data
     * @param array $rules
     * @return bool
     * @throws ApiException
     */
    public static function validate($data, $rules)
    {
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            throw new ApiException($validator->messages(), 400);
        }
        return true;
    }
}