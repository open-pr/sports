<?php
/**
 * Created by PhpStorm.
 * User: pablo.juarez
 * Date: 10/12/2018
 * Time: 3:31 PM
 */

namespace App\Validate;


class PlayerValidate
{
    use Validate;

    /**
     * @param $data
     * @return array
     * @throws \App\Exceptions\ApiException
     */
    public static function newPlayer($data)
    {
        $rules = [
            'firstName' => 'required',
            'lastName' => 'required',
            'alias' => 'present',
            'team' => 'nullable|exists:teams,id',
        ];
        Validate::validate($data, $rules);

        return [
            'first_name' => $data['firstName'],
            'last_name' => $data['lastName'],
            'alias' => $data['alias'],
            'team' => $data['team'],
        ];
    }

    /**
     * @param $data
     * @return bool
     * @throws \App\Exceptions\ApiException
     */
    public static function updatePlayer($data)
    {
        $rules = [
            'player' => 'required|exists:players,id',
            'firstName' => 'nullable',
            'lastName' => 'nullable',
            'alias' => 'nullable',
            'team' => 'nullable|exists:teams,id',
        ];
        Validate::validate($data, $rules);
        return true;
    }

    /**
     * validate if a player exists by an id given
     *
     * @param $playerId
     * @return bool
     * @throws \App\Exceptions\ApiException
     */
    public static function exists($playerId)
    {
        $data = ['id' => $playerId];
        $rules = ['id' => 'exists:players'];
        Validate::validate($data, $rules);
        return true;
    }

}