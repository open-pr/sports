<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 12/10/18
 * Time: 07:06 AM
 */

namespace App\Common\Util;

use Illuminate\Http\Response;

class ApiResponse
{
    /**
     * Wrap Response on a single call
     *
     * @param array $data
     * @param int $code
     * @param array $headers
     * @param bool $setEtag
     * @return Response
     */
    static function wrap($data, $code, $headers = [], $setEtag = false) {
        if (is_null($data)) {
            return self::emptyWrap();
        }
        $response = new Response();
        if (array_key_exists('data', $data)) {
            $response->setContent($data);
        } else {
            $response->setContent([ 'data' => $data ]);
        }
        $response->setStatusCode($code);

        foreach ($headers as $index => $header) {
            $response->header($index, $header);
        }

        if ($setEtag) {
            $response->setEtag(md5($response->getContent()));
        }

        return $response;

    }


    /**
     * @param array|string $data
     * @param int $code
     * @param string $title
     * @return Response
     */
    static function wrapError($data, $code = 400, $title = 'Bad Request') {
        $error = [
            'errors' => [
                'status' => $code,
                'source' => '',
                'title' => $title,
                'detail' => $data
            ]
        ];
        $response = new Response();
        $response->setContent($error);
        $response->setStatusCode($code);
        return $response;
    }

    /**
     * @return Response
     */
    private static function emptyWrap () {
        $error = [
            'errors' => [
                'status' => 404,
                'source' => '',
                'title' => 'resource not found',
                'detail' => 'resource requested does not exists'
            ]
        ];
        $response = new Response();
        $response->setContent($error);
        $response->setStatusCode(404);
        return $response;
    }

}
