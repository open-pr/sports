<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 12/10/18
 * Time: 07:06 AM
 */

namespace App\Common\Util;


use App\Exceptions\ApiException;

class Assert
{
    /**
     * Validate id input
     *
     * @param $id
     * @return bool
     * @throws ApiException
     */
    static function id($id)
    {
        $value = (int)$id;
        if (is_integer($value) && $value > 0 ) {
            return true;
        }
        throw new ApiException("identifier value expected, instead {$id}", 400);
    }
}
