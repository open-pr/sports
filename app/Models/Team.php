<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Team
 *
 * @property $id
 * @property $name
 * @property $created_at
 * @property $updated_at
 * @package App\Models
 */
class Team extends Model
{
    protected $fillable = ['name'];
}
