<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Player
 *
 * @property $id
 * @property $first_name
 * @property $last_name
 * @property $alias
 * @property $team
 * @property $created_at
 * @property $updated_at
 *
 * @package App\Models
 */
class Player extends Model
{
    protected $fillable = ['first_name', 'last_name', 'alias', 'team'];

    public function teamInfo()
    {
        return $this->hasOne('App\Models\Team', 'id');
    }
}
