<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class HasToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!array_key_exists('authorization', $request->header())) {

            return response([
                'errors' => [
                    'status' => 401,
                    'source' => '',
                    'title' => 'Unauthorized',
                    'detail' => 'Missing token'
                ]

            ], 401);
        }

        if (!$this->guard()->check()) {
            return response([
                'errors' => [
                    'status' => 401,
                    'source' => '',
                    'title' => 'Unauthorized',
                    'detail' => 'Invalid token'
                ],
            ], 401);
        }

        return $next($request);
    }

    protected function guard() {
        return Auth::guard();
    }
}
