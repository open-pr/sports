<?php

namespace App\Http\Controllers;

use App\Common\Util\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials  = $request->only('email','password');
        if ($token = $this->guard()->attempt($credentials)) {
            return response('', 201)->header('Location', $token);
        }
        return ApiResponse::wrapError('Invalid Credentials', 401, 'Unauthorized');
    }


    /**
     * Get the guard to be used during authentication.
     *
     * @return mixed
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
