<?php

namespace App\Http\Controllers;

use App\Common\Util\Assert;
use App\Dao\PlayerDao;
use App\Http\Resources\PlayerResource;
use App\Validate\PlayerValidate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PlayerController extends Controller
{
    protected $response;

    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    /**
     * @param Request $request
     * @return PlayerResource
     * @throws \App\Exceptions\ApiException
     */
    public function store(Request $request)
    {
        $payload = PlayerValidate::newPlayer($request->input());
        $player = PlayerDao::store($payload);
        $jsonApi = new PlayerResource($player);
        $jsonApi->statusCode = 201;
        return $jsonApi;
    }

    /**
     * Retrieve a player by their id
     * @param $id
     * @return PlayerResource
     * @throws \App\Exceptions\ApiException
     */
    public function getById($id)
    {
        Assert::id($id);
        PlayerValidate::exists($id);
        $player = PlayerDao::getById($id);
        $jsonApi = new PlayerResource($player);
        $jsonApi->etag = true;
        return $jsonApi;
    }

    /**
     * Update player by an id given
     *
     * @param Request $request
     * @param $id
     * @return Response
     * @throws \App\Exceptions\ApiException
     */
    public function update(Request $request, $id)
    {
        $payload = $request->input();
        $payload['player'] = $id;
        PlayerValidate::updatePlayer($payload);
        PlayerDao::update($id, $request->input());
        $this->response->setStatusCode(204);
        return $this->response;
    }
}
