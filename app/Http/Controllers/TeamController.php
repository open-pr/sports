<?php

namespace App\Http\Controllers;

use App\Common\Util\Assert;
use App\Dao\TeamDao;
use App\Http\Resources\TeamResource;
use App\Team;
use App\Validate\TeamValidate;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //TODO retrieve all teams ?
    }

    /**
     * @param Request $request
     * @return TeamResource
     * @throws \App\Exceptions\ApiException
     */
    public function store(Request $request)
    {
        TeamValidate::newTeam($request->input());
        $team = TeamDao::store($request->input());
        $jsonApi = new TeamResource($team);
        $jsonApi->statusCode = 201;
        return $jsonApi;
    }

    /**
     * Return a team entity
     *
     * @param $id
     * @return TeamResource
     * @throws \App\Exceptions\ApiException
     */
    public function getById($id)
    {
        Assert::id($id);
        TeamValidate::exists($id);
        $team = TeamDao::getById($id);
        $jsonApi = new TeamResource($team);
        $jsonApi->etag = true;
        return $jsonApi;
    }

}
