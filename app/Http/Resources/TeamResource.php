<?php

namespace App\Http\Resources;

use App\Dao\PlayerDao;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class TeamResource
 *
 * @property $id
 * @property $name
 * @property $created_at
 * @property $updated_at
 * @package App\Http\Resources
 */
class TeamResource extends Resource
{
    use BaseResource;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $players = PlayerDao::getByTeam((int)$this->id);
        return [
            'type'          => 'team',
            'id'            => (int)$this->id,
            'attributes'    => [
                'name' => $this->name,
                'players' => $players,
                'created' => $this->created_at,
                'updated' => $this->updated_at
            ],
            'links'         => [
                'self' => route('team.show', ['teams' => $this->id]),
            ],
        ];
    }

    public function withResponse($request, $response)
    {
        $this->parseWithResponse($request, $response);
    }
}
