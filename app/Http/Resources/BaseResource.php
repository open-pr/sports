<?php
/**
 * Created by PhpStorm.
 * User: pablo.juarez
 * Date: 10/12/2018
 * Time: 2:36 PM
 */

namespace App\Http\Resources;


trait BaseResource
{
    /**
     * @var null|int http status code
     */
    public $statusCode = null;
    /**
     * @var bool whether or not generate a ETAG
     */
    public $etag = false;
    /**
     * @var array http headers to be delivered on response
     */
    public $headers = [];

    /**
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Http\JsonResponse $response
     */
    public function parseWithResponse($request, $response)
    {
        if ($this->etag) {
            $response->setEtag(md5($response->getContent()));
        }
        if (!is_null($this->statusCode)) {
            $response->setStatusCode($this->statusCode);
        }

        foreach ($this->headers as $index => $header) {
            $response->header($index, $header);
        }
    }
}