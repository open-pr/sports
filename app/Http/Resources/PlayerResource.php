<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

/**
 * Class PlayerResource
 *
 * @property $id
 * @property $first_name
 * @property $last_name
 * @property $alias
 * @property $team
 * @property $teamInfo
 * @property $created_at
 * @property $updated_at
 *
 * @package App\Http\Resources
 */
class PlayerResource extends Resource
{
    use BaseResource;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $base = [
            'type'          => 'player',
            'id'            => (int)$this->id,
            'attributes'    => [
                'name' => $this->first_name,
                'lastName' => $this->last_name,
                'alias' => $this->alias,
                'team' => $this->team,
                'created' => $this->created_at,
                'updated' => $this->updated_at
            ],
            'links'         => [
                'self' => route('team.show', ['teams' => $this->id]),
            ],
        ];
        if (!is_null($this->team)) {
            $base['attributes']['team'] = $this->teamInfo;
        }
        return $base;
    }

    public function withResponse($request, $response)
    {
        $this->parseWithResponse($request, $response);
    }
}
