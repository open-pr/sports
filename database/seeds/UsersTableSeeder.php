<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Api user',
            'email' => 'username@sportsapi.com',
            'password' => '$2y$10$g2HnZyxs7ZbV3EXRststbe..fIL6xOwUIygRBTTuJSO6cWrNQeLKa', //lolo123
        ]);
    }
}
