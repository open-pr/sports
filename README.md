# Open sports API
Restish flavored API to add sports teams and their players  

## Api  
This api was build with [Laravel](https://laravel.com/docs/5.5)

### prerequisites
- GIT [download](https://git-scm.com/download/win)
- Composer [download](https://getcomposer.org/download/)


### Setting up
once cloned this repo is time to install the dpeendencies for this project, to achive that, just run  
`composer install`
<br>
if everything goes ok, is time to create a database for this project, please create a mysql/MariaDb database called 'sports'  
once database is created is time to migrate our DB structure, for that, run this  
`php artisan migrate`  
Now we should have migrated our DB schema, and sinsce our db is empty, let's seed some data, to that, run
`php artisan db:seed`  
By now we should have also data on our db, by now just remain update our main config file, please locate  
the file .env.example, copy the content and create new file called '.env', paste the content from .evn.example
update the content for DB_DATABASE for 'sports' and DB_USERNAME, DB_PASSWORD accordingly your DB credentials,
and then we can start our project, finally :)  

### Run
`php artisan artisan serve`  
now the project will be available at http://localhost:8000  

### Api request
now is time to fire up some requests to our shiney API, you can find here more info on how to send request  
[API Documentation](https://documenter.getpostman.com/view/128629/RWgrwxQa)

### Api authentication
Be aware that this API is protected to accept only request from trusted user, to acompplish any request, we need  
to send every time a bearer token, this token is obtained by consume the login endpoint, the token is located on  
the http headers, specifically the 'location' header.
