<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api'], function () {
    /**
     * Login
     * fields: { email: "" , password: ""}
     */
    Route::post('login', 'AuthController@login');

    /**
     * routes protected by token
     */
    Route::group(['middleware' => 'has.token'], function () {

        /**
         * Routes related to teams resources
         */
        Route::group(['prefix' => 'teams'], function () {
            Route::post('/', 'TeamController@store');
            Route::get('/{id}', 'TeamController@getById')->name('team.show');

        });

        /**
         * Routes related to players resources
         */
        Route::group(['prefix' => 'players'], function () {
            Route::post('/', 'PlayerController@store');
            Route::get('/{id}', 'PlayerController@getById')->name('player.show');
            Route::patch('/{id}', 'PlayerController@update');
        });
    });
});
